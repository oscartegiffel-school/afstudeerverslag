@startuml

class Repository {
    
}

class MetricValueRepository {
    +get (DimensionEnumeration dimensionType, String dimensionValueId, Metric $metric) 
    -metricFilterToSql(MetricFilter $filter)
}

class DimensionValueRepository {
    +get (Dimension $dimension) 
    -dimensionFilterToSql (DimensionFilter $filter)
}

MetricValueRepository -up--|> Repository
DimensionValueRepository -up--|> Repository

@enduml 
