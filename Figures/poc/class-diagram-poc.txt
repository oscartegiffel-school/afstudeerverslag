@startuml

class DimensionEnumeration

class MetricEnumeration

abstract class Dimension {
    -DimensionEnumeration type
    -ArrayList DimensionFilters
}

class DimensionFilter {
    -String field
    -DimensionFilterOperator operator
    -ArrayList expressions
    +getField()
    +getOperator()
    +getExpressions()
}

abstract class Metric {
    -MetricEnumeration metricName
    -ArrayList MetricFilters
    +getMetricName()
    +getMetricFilters()
}

class MetricFilter {
    -String field
    -MetricFilterOperator operator
    -String comparisonValue
    +getField()
}

Enumeration <|-- DimensionEnumeration
Enumeration <|-- MetricEnumeration

' DimensionEnumeration -up--|> Enumeration
' MetricEnumeration -up--|> Enumeration

DimensionEnumeration -up--* Dimension
DimensionFilter "*" -up--* "1" Dimension

MetricEnumeration -up--* Metric
MetricFilter "*" -up--* "1" Metric


@enduml 