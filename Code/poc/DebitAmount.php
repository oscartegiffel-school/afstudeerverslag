<?php

    protected static $table = 'journaalregel';

    protected static $fieldMap = [
        'general_ledger_account_id' => 'grootboekrekening_id',
        'entry_date' => 'boekdatum',
        'type' => 'type',
        'journal_entry_id' => 'journaalpost_id',
        'amount' => 'bedrag',
    ];

    protected static $relationMap = [
        Dimension::GENERAL_LEDGER_ACCOUNT => 'general_ledger_account_id'
    ];

    /**
     * DebitAmount constructor.
     * @param ArrayList $metricFilters
     */
    public function __construct (ArrayList $metricFilters) {
        parent::__construct(Metric::debitAmount(), $metricFilters);
    }

    public function metricFunction (): MetricFunction {
        return new MetricFunction(AggregateFunction::sum(), 'bedrag');
    }