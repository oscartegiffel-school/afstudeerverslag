<?php
public function get (DimensionDefinition $dimensionDefinition, string $dimensionValueId, MetricDefinition $metricDefinition): int {
        $metricFilters = $metricDefinition->getMetricFilters()->copy();

        // Exceptions

        $this->handleExceptions($metricFilters, $dimensionDefinition);


        //  This is the aggregate function to get the value calculate function
        $select = "SELECT " . $this->metricFunctionToSql($metricDefinition->metricFunction());
        $from = " FROM " . $metricDefinition->getTable();

        if ($metricDefinition->getMetric()->isDebitAmount()) {
            $debitAmountFilter = new MetricFilter(
                "type",
                MetricFilterOperator::equal(),
                new ArrayList("1")
            );

            $metricFilters->add($debitAmountFilter);

        } else if ($metricDefinition->getMetric()->isCreditAmount()) {
            // Add a filter to only sum the credit
            $creditAmountFilter = new MetricFilter(
                "type",
                MetricFilterOperator::equal(),
                new ArrayList("2")
            );

            $metricFilters->add($creditAmountFilter);
        }

        // If the dimension is general ledger account add a filter to only sum
        // with a relation to the general ledger account
        $metricDefinition->assertInRelationMap($dimensionDefinition->getDimension());

        $dimensionFilter = new MetricFilter(
        // Get the foreign key of the relation
            $metricDefinition->getRelationMap()[$dimensionDefinition->getDimension()->value()],
            MetricFilterOperator::equal(),
            new ArrayList($dimensionValueId)
        );
        $metricFilters->add($dimensionFilter);

        // Transform the metric filters to where clauses
        $whereClauses = $metricFilters->map(function (MetricFilter $filter) use ($metricDefinition) {
            return $this->metricFilterToSql($metricDefinition, $filter);
        });

        // When the array is not empty glue the array together with AND clauses
        if (!empty($whereClauses)) {
            $where = " WHERE " . $whereClauses->join(" AND ");
        }

        // Glue the query together
        $query = $select . $from . $where;

        // Execute the query and return the first result (there is only 1 result because aggregation function)

        try {
            $result = $this->execute($query)->first();
        } catch (\Exception $exception) {
            throw new MoneyMonkException(ReportingErrorCode::problemWithQuery());
        }

        // Return the first value of the associative array because there is only one result
        // A metric is always an aggregation and returns 1 result and 1 value
        // The value of a metric is always an int
        return (int)reset($result);
    }
}