<?php

protected function execute (): Report {

        // Get all id's off the dimension
        $dimensionValues = DimensionValuesRepository::getInstance()
            ->get($this->dimensionDefinition);

        // Create an empty report
        $report = new Report($dimensionValues);

        // Loop over all metrics
        foreach ($this->metrics as $metric) {

            $dimensionValues = $report->getDimensionValues();
            // Create an empty array map for the metric table
            $metricTable = new ArrayMap();

            // Loop over all dimension value id's
            foreach ($dimensionValues as $dimensionValue) {
                // Get all metric values based on the dimension value id
                $metricValue = MetricValueRepository::getInstance()
                    ->get($this->dimensionDefinition, $dimensionValue, $metric);

                $metricTable->put($dimensionValue, $metricValue);
            }

            // Append the array map to the report
            $report->append($metricTable);
        }

        return $report;
    }