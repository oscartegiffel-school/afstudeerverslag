<?php

class DimensionValueRepository {
    public function get (DimensionDefinition $dimension) {

        if ($dimension instanceof GeneralLedgerAccountDimensionDefinition) {
            $query = "SELECT grootboekrekening_id FROM grootboekrekening "; // General Ledger account table
        } 

        $whereClauses = [];

        /** @var DimensionFilter $dimensionFilter */
        foreach ($dimension->getDimensionFilters() as $dimensionFilter) {
            $whereClauses[] = $this->dimensionFilterToSql($dimensionFilter);
        }

        if (!empty($whereClauses)) {
            $where = " WHERE " . implode(" AND ", $whereClauses);
        }

        $query .= $where;

        $rows = $this->execute($query);

        $result = [];
        foreach ($rows as $row) {
            $result[] = $row["grootboekrekening_id"];
        }

        return $result;
    }
}
    