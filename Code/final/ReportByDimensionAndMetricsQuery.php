<?php

class ReportByDimensionAndMetrics {
    protected function execute (): Report {
        $this->addAdministrationDimensionFilter();

        return $this->dimension->getReport($this->metrics);
    }
}