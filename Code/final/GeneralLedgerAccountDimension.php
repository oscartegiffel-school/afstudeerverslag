<?php

class GeneralLedgerAccountDimension {
    protected static $acceptedFilterFields = [
        'administration_id',
        'category'
    ];

    /**
     * @return Report
     * @throws MoneyMonkException
     * @throws \moneymonk\database\DatabaseException
     */
    protected function initializeReport(): Report
    {
        $report = new Report();

        $filters = $this->preProcessFilters();
        $where = FiltersToSqlService::getInstance()->execute($filters);
        $sql = "SELECT grootboekrekening_id FROM grootboekrekening $where";
        $resultSet = $this->getResultSet($sql);

        foreach ($resultSet as $row) {
            $report->put($row['grootboekrekening_id'], new ArrayList());
        }

        return $report;
    }

    /**
     * @return ArrayList
     * @throws MoneyMonkException
     */
    public function preProcessFilters(): ArrayList
    {
        $newFilters = new ArrayList();
        /** @var DimensionFilter $filter */
        foreach ($this->getFilters() as $filter) {
            $this->isAcceptedFilterField($filter->getField());
            switch ($filter->getField()) {
                case 'administration_id':
                    $administrationFilter = new DimensionFilter('administratie_id', $filter->getOperator(),
                        $filter->getExpressions());
                    $newFilters->add($administrationFilter);
                    break;
                case 'category':
                    $categoryFieldMap = [
                        "INCOME" => 'omzet',
                        "EXPENSE" => 'kosten'
                    ];

                    $expressions = $filter->getExpressions()->map(function (string $expression) use ($categoryFieldMap
                    ) {
                        return $categoryFieldMap[$expression];
                    });

                    $categoryFilter = new DimensionFilter('grootboekrekening.categorie', $filter->getOperator(),
                        $expressions);
                    $newFilters->add($categoryFilter);
                    break;
                default:
                    throw new MoneyMonkException(ReportingErrorCode::filterFieldNotExcepted($filter->getField()));
            }
        }

        return $newFilters;
    }
}