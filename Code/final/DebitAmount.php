<?php

class DebitAmount extends Metric
{

    // ... 

    public function getReport(Dimension $dimension): Report
    {
        $report = new Report();

        if ($dimension instanceof GeneralLedgerAccountDimension) {
            $filters = new ArrayList();

            $debitAmountFilter = new MetricFilter(
                "type",
                MetricFilterOperator::equal(),
                new ArrayList("1")
            );

            $this->getFilters()->add($debitAmountFilter);

            $metricFilters = $this->preProcessFilters($dimension);

            $dimensionFilters = $dimension->preProcessFilters($this);

            $filters->addList($metricFilters);
            $filters->addList($dimensionFilters);

            $where = FiltersToSqlService::getInstance()->execute($filters);

            $sql = "SELECT grootboekrekening.grootboekrekening_id, SUM(journaalregel.bedrag) 
                    FROM grootboekrekening
                    LEFT join journaalregel ON grootboekrekening.grootboekrekening_id = journaalregel.grootboekrekening_id
                    LEFT join journaalpost ON journaalpost.journaalpost_id = journaalregel.journaalpost_id
                    $where 
                    GROUP BY journaalregel.grootboekrekening_id";
            $report = $this->executeSql($sql);
        }

        return $report;
    }
}