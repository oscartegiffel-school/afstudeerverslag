<?php

abstract class Dimension {
    
    public function getReport(ArrayList $metrics): Report
    {

        $report = $this->initializeReport();

        /** @var Metric $metric */
        foreach ($metrics as $metric) {
            $metric->isAcceptedDimension($this);
            $metricReport = $metric->getReport($this);
            $report->append($metricReport);
        }

        return $report;
    }
}