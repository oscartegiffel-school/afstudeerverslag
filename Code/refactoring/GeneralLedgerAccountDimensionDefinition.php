<?php
class GeneralLedgerAccountDimensionDefinition extends DimensionDefinition {
    protected $table = 'grootboekrekening';

    protected $fieldMap = [
        'general_ledger_account_id' => 'grootboekrekening_id',
        'administration_id' => 'administratie_id',
        'category' => 'categorie',
    ];

    protected $metricRelations = [
        Metric::DEBIT_AMOUNT,
        Metric::CREDIT_AMOUNT,
    ];

    public function __construct (ArrayList $filter) {
        parent::__construct(Dimension::generalLedgerAccount(), $filter);
    }
}