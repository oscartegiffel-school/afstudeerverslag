<?php

class ReportByDimensionAndMetricsQuery
{
    protected function execute(): array
    {
        // Serialize and hash the dimension and metrics to create a unique id
        $hash = sha1(serialize($this->dimension) . serialize($this->metrics));
        $apiCacheKey = ApiCacheKey::reportHash($this->administrationId, $hash);
        $apiCacheField = ApiCacheField::allOfAdministration();

        $predisCache = new PredisApiCache();

        if ($this->useCache && $predisCache->exists($apiCacheKey, $apiCacheField)) {
            $reportData = $predisCache->get($apiCacheKey, $apiCacheField);
        } else {
            $report = $this->dimension->getReport($this->metrics);
            $reportData = ReportToApiDataMapper::getInstance()
            ->execute($report);
            $predisCache->set($apiCacheKey, $apiCacheField, $reportData, 3600);
        }

        return $reportData;
    }
    
}
